<?php
/*
 * track_failures.php
 * Handles HTTP callback POST data for Mailgun failure tracking
 * See http://documentation.mailgun.net/user_manual.html#tracking-failures
 * Don Seiler, don.seiler@enkitec.com
 */

// Include common variables and functions
include 'mailgun_lib.php';

// Read and sanitize POST data from Mailgun
$event = $_REQUEST["event"];
$recipient = $_REQUEST["recipient"];
//$domain = $_REQUEST["domain"];
$message_headers = $_REQUEST["message-headers"];
//$reason = $_REQUEST["reason"];
//$code = $_REQUEST["code"];
$description = $_REQUEST["description"];
$timestamp = $_REQUEST["timestamp"];
$token = $_REQUEST["token"];
$signature = $_REQUEST["signature"];
if (isset($_REQUEST["email_id"])) $email_id = $_REQUEST["email_id"];
if (isset($_REQUEST["send_date"])) $send_date = $_REQUEST["send_date"];
//$tag = $_REQUEST["X-Mailgun-Tag"];

if(isset($email_id) && verify($token, $timestamp, $signature)) {
	add_log($email_id, $recipient, $send_date, $event, $description, date(DATE_FORMAT,$timestamp));
} else {
	error_log("Mailgun message headers: " . $message_headers); // XXX
	if(!isset($email_id)) {
		error_log("Mailgun failure trackback for recipient $recipient timestamp $timestamp has no email_id.");
	} else {
		error_log("Mailgun failure trackback for recipient $recipient timestamp $timestamp failed verification.");
	}
}
?>
