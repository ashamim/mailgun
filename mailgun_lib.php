<?php
/*
 * track_deliveries.php
 * Handles HTTP callback POST data for Mailgun tracking
 * Don Seiler, don.seiler@enkitec.com
 */

// Define API_KEY as constant
define('API_KEY','key-58mlwep9mehvd5dxj7fyblsx2542j196');

// Define standard date format for display and Oracle interatction
// e.g. print(date(DATE_FORMAT, $timestamp));
//define('DATE_FORMAT','Y/m/d H:i:s');
define('DATE_FORMAT','YmdHis');

/*
 * Connects to WWEX Oracle database and inserts tracking data.
 */
function add_log($in_email_id, $in_recipient, $in_sent_date, $in_status
	, $in_error_message, $in_callback_date) {

	// I think we need to use oci_new_connect to ensure transaction isolatopm,
	// as using oci_connect seems to lack it
	$conn = oci_new_connect('wexnet', 'wexnet1', '(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=db3-vip)(PORT=1521))(ADDRESS=(PROTOCOL=TCP)(HOST=db4-vip)(PORT=1521))(LOAD_BALANCE=yes)(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=wwex)))');

	$sql = "INSERT INTO wexnet.email_logs (email_id, recipient, sent_date, status, error_message, callback_date) " .
		"VALUES (:email_id,lower(:recipient),to_date(:sent_date,'yyyymmddhh24miss'),lower(:status),:error_message,to_date(:callback_date,'yyyymmddhh24miss'))";

	$stmt = oci_parse($conn, $sql);
	oci_bind_by_name($stmt, ':email_id', $in_email_id);
	oci_bind_by_name($stmt, ':recipient', $in_recipient);
	oci_bind_by_name($stmt, ':sent_date', $in_sent_date);
	oci_bind_by_name($stmt, ':status', $in_status);
	oci_bind_by_name($stmt, ':error_message', $in_error_message);
	oci_bind_by_name($stmt, ':callback_date', $in_callback_date);

	oci_execute($stmt);
	//oci_commit($conn);
	oci_close($conn);
}

/*
 * Verifies messages as authentic Mailgun callbacks
 * See http://documentation.mailgun.net/user_manual.html#securing-webhooks
 */
function verify($token, $timestamp, $signature) {
	$hash = hash_hmac('sha256', $timestamp . $token, API_KEY);
	if ($hash == $signature) {
		return true;
	} else {
		return false;
	}
}


/*
 * Parses the JSON meassag-headers value returned from Mailgun
 * and returns an associate array of the value we are interested in
 */
function parse_headers($message_headers) {
	$json = json_decode($message_headers);

	$headers = array(
		"Received" => null,
		"Reply-To" => null,
		"From" => null,
		"To" => null,
		"Cc" => null,
		"Bcc" => null,
		"Date" => null,
		"Subject" => null,
		"X-Mailgun-Tag" => null,
		"X-Mailgun-Variables" => null,
		"Sender" => null
	);

	foreach ($json as $id => $data) {
		foreach ($data as $key => $value) {
			// For each array, the key is the value of index 0,
			// and the value is the value of index 1
			//echo "$key = $value\n";
			if ($key == 0) {
				switch ($value) {
         				case 'Sender':
						$headers['Sender'] .= $data[1] . " ";
						break;
         				case 'Received':
						$headers['Received'] .= $data[1] . " ";
						break;
         				case 'Reply-To':
						$headers['Reply-To'] .= $data[1] . " ";
						break;
         				case 'From':
						$headers['From'] .= $data[1] . " ";
						break;
         				case 'To':
						$headers['To'] .= $data[1] . " ";
						break;
         				case 'Cc':
						$headers['Cc'] .= $data[1] . " ";
						break;
         				case 'Bcc':
						$headers['Bcc'] .= $data[1] . " ";
						break;
         				case 'Date':
						$headers['Date'] .= $data[1] . " ";
						break;
         				case 'Subject':
						$headers['Subject'] .= $data[1] . " ";
						break;
         				case 'X-Mailgun-Tag':
						$headers['X-Mailgun-Tag'] .= $data[1] . " ";
						break;
         				case 'X-Mailgun-Variables':
						$headers['X-Mailgun-Variables'] .= $data[1] . " ";
						break;
				}
				continue;
			}
		}
	}

	// Trim that trailing whitespace
	foreach($headers as &$header) {
		$header = rtrim($header);
	}

	return $headers;
}
?>
