<?php
/*
 * track_deliveries.php
 * Handles HTTP callback POST data for Mailgun tracking
 * See http://documentation.mailgun.net/user_manual.html#tracking-deliveries
 * Don Seiler, don.seiler@enkitec.com
 */

// Include common variables and functions
include 'mailgun_lib.php';

//error_log(var_export($_REQUEST, true), 3, "/tmp/mailgun_trackback.log");


// Read and sanitize POST data from Mailgun
$event = $_REQUEST["event"];
$recipient = $_REQUEST["recipient"];
//$domain = $_REQUEST["domain"];
$message_headers = $_REQUEST["message-headers"];
//$message_id = $_REQUEST["Message-Id"];
$timestamp = $_REQUEST["timestamp"];
$token = $_REQUEST["token"];
$signature = $_REQUEST["signature"];
if (isset($_REQUEST["email_id"])) $email_id = $_REQUEST["email_id"];
if (isset($_REQUEST["send_date"])) $send_date = $_REQUEST["send_date"];
//$tag = $_REQUEST["X-Mailgun-Tag"];
$description = "";

if(isset($email_id) && verify($token, $timestamp, $signature)) {
	add_log($email_id, $recipient, $send_date, $event, $description, date(DATE_FORMAT,$timestamp));
} else {
	error_log("Mailgun message headers: " . $message_headers); // XXX
	if(!isset($email_id)) {
		error_log("Mailgun delivery trackback for recipient $recipient timestamp $timestamp has no email_id.");
	} else {
		error_log("Mailgun delivery trackback for recipient $recipient timestamp $timestamp failed verification.");
	}
}
?>
